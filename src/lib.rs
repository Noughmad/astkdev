extern crate proc_macro2;
extern crate quote;
extern crate syn;

#[macro_use]
extern crate log;
extern crate pretty_env_logger;

mod astredux;
pub use astredux::*;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
