use proc_macro2::{LineColumn, Span};
use syn::{parse_file, Expr, Field, File, Ident, ImplItem, Item, ItemType, Local, Pat, Path,
          PathSegment, ReturnType, Stmt, Type, UseTree, Variant, spanned::Spanned};
use quote::ToTokens;

use pretty_env_logger;

use std::ffi::{CStr, CString};
use std::ptr;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum RSNodeKind {
    Crate = 0,
    Module = 1,
    StructDecl = 2,
    EnumDecl = 3,
    TraitDecl = 4,
    ImplDecl = 5,
    TypeAliasDecl = 6,
    FieldDecl = 7,
    EnumVariantDecl = 8,
    FunctionDecl = 9,
    ParmDecl = 10,
    VarDecl = 11,
    Path = 12,
    PathSegment = 13,
    Block = 14,
    Arm = 15,
    Unexposed = 16,
}

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum RSDiagnosticLevel {
    Info = 0,
    Note = 1,
    Warning = 2,
    Error = 3,
    Fatal = 4,
}
#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum RSVisitResult {
    Break = 0,
    Continue = 1,
    Recurse = 2,
}
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct RSLocation {
    pub line: ::std::os::raw::c_int,
    pub column: ::std::os::raw::c_int,
}

impl From<LineColumn> for RSLocation {
    fn from(lc: LineColumn) -> RSLocation {
        RSLocation {
            line: lc.line as i32,
            column: lc.column as i32,
        }
    }
}

impl RSLocation {
    pub fn invalid() -> RSLocation {
        RSLocation {
            line: -1,
            column: -1,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct RSRange {
    pub start: RSLocation,
    pub end: RSLocation,
}

impl From<Span> for RSRange {
    fn from(span: Span) -> RSRange {
        RSRange {
            start: span.start().into(),
            end: span.end().into(),
        }
    }
}

impl RSRange {
    pub fn invalid() -> RSRange {
        RSRange {
            start: RSLocation::invalid(),
            end: RSLocation::invalid(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone)]
pub struct RSCrate {
    name: String,
    source: String,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Node {
    None,
    File(File),
    Item(Item),
    Field(Field),
    Variant(Variant),
    ImplItem(ImplItem),
    Type(Type),
    Path(Path),
    PathSegment(PathSegment),
    Expr(Expr),
    Local(Local),
}

impl Node {
    fn get_ident(&self) -> Option<&Ident> {
        match &self {
            Node::Item(item) => match &item {
                Item::ExternCrate(e) => Some(&e.ident),
                Item::Use(u) => match &u.tree {
                    UseTree::Path(p) => Some(&p.ident),
                    UseTree::Name(n) => Some(&n.ident),
                    UseTree::Rename(r) => Some(&r.rename),
                    _ => None,
                },
                Item::Fn(f) => Some(&f.ident),
                Item::Struct(s) => Some(&s.ident),
                Item::Enum(e) => Some(&e.ident),
                Item::Type(t) => Some(&t.ident),
                Item::Trait(t) => Some(&t.ident),
                Item::Macro(m) => m.ident.as_ref(),
                Item::Macro2(m) => Some(&m.ident),
                _ => None,
            },
            Node::Field(f) => f.ident.as_ref(),
            Node::Variant(v) => Some(&v.ident),
            Node::ImplItem(item) => match &item {
                ImplItem::Method(m) => Some(&m.sig.ident),
                ImplItem::Type(t) => Some(&t.ident),
                _ => None,
            },
            Node::Type(t) => match &t {
                Type::Path(p) => None,
                _ => None,
            },
            Node::PathSegment(s) => Some(&s.ident),
            Node::Local(l) => {
                if let Some(Pat::Ident(i)) = l.pats.iter().next() {
                    Some(&i.ident)
                } else {
                    None
                }
            }
            Node::Expr(e) => None,
            _ => None,
        }
    }

    fn get_span(&self) -> Option<Span> {
        match self {
            Node::Item(i) => Some(i.span()),
            Node::Field(f) => Some(f.span()),
            Node::Variant(v) => Some(v.span()),
            Node::ImplItem(i) => Some(i.span()),
            Node::Type(t) => Some(t.span()),
            Node::Path(p) => Some(p.span()),
            Node::PathSegment(s) => Some(s.span()),
            Node::Local(l) => Some(l.span()),
            Node::Expr(e) => Some(e.span()),
            _ => None,
        }
    }
}

#[repr(C)]
#[derive(Debug)]
pub struct RSNode<'a> {
    crate_: &'a mut RSCrate,
    node: Node,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct RSDiagnostic {
    _unused: [u8; 0],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct RSDiagnosticIterator {
    _unused: [u8; 0],
}

pub type CallbackFn = ::std::option::Option<
    unsafe extern "C" fn(
        node: *const RSNode,
        parent: *const RSNode,
        data: *mut ::std::os::raw::c_void,
    ) -> RSVisitResult,
>;

#[no_mangle]
pub unsafe extern "C" fn parse_crate(
    name: *const ::std::os::raw::c_char,
    source: *const ::std::os::raw::c_char,
) -> *mut RSCrate {
    pretty_env_logger::try_init().ok();
    info!(
        "Parse crate: {}, size = {}",
        CStr::from_ptr(name).to_string_lossy(),
        CStr::from_ptr(source).to_bytes().len()
    );

    let rs_crate = Box::new(RSCrate {
        name: CStr::from_ptr(name).to_string_lossy().into_owned(),
        source: CStr::from_ptr(source).to_string_lossy().into_owned(),
    });
    Box::into_raw(rs_crate)
}

#[no_mangle]
pub unsafe extern "C" fn destroy_crate(crate_: *mut RSCrate) {
    Box::from_raw(crate_);
}

#[no_mangle]
pub unsafe extern "C" fn node_from_crate<'a>(crate_: *mut RSCrate) -> *mut RSNode<'a> {
    let rs_node = Box::new(RSNode {
        crate_: &mut *crate_,
        node: if let Ok(f) = parse_file(&(*crate_).source) {
            Node::File(f)
        } else {
            Node::None
        },
    });

    info!("Node from crate");

    Box::into_raw(rs_node)
}

#[no_mangle]
pub unsafe extern "C" fn destroy_node(node: *mut RSNode) {
    Box::from_raw(node);
}

#[no_mangle]
pub unsafe extern "C" fn node_get_id(node: *mut RSNode) -> ::std::os::raw::c_uint {
    0
}

#[no_mangle]
pub unsafe extern "C" fn node_get_crate(node: *const RSNode) -> *const RSCrate {
    (*node).crate_
}

#[no_mangle]
pub unsafe extern "C" fn node_get_kind(node: *mut RSNode) -> RSNodeKind {
    let kind = match &(*node).node {
        Node::Item(item) => match item {
            Item::Mod(_) => RSNodeKind::Module,
            Item::Struct(_) => RSNodeKind::StructDecl,
            Item::Enum(_) => RSNodeKind::EnumDecl,
            Item::Trait(_) => RSNodeKind::TraitDecl,
            Item::Impl(_) => RSNodeKind::ImplDecl,
            Item::Type(_) => RSNodeKind::TypeAliasDecl,
            Item::Fn(_) => RSNodeKind::FunctionDecl,
            // TODO: Add kinds that are not items but parts of an item
            _ => RSNodeKind::Unexposed,
        },
        Node::File(file) => RSNodeKind::Crate,
        Node::Field(f) => RSNodeKind::FieldDecl,
        Node::Variant(f) => RSNodeKind::EnumVariantDecl,
        Node::ImplItem(item) => match item {
            ImplItem::Method(_) => RSNodeKind::FunctionDecl,
            ImplItem::Type(_) => RSNodeKind::TypeAliasDecl,
            _ => RSNodeKind::Unexposed,
        },
        Node::Type(Type::Path(_)) => RSNodeKind::Path,
        Node::Path(_) => RSNodeKind::Path,
        Node::PathSegment(_) => RSNodeKind::PathSegment,
        Node::Local(_) => RSNodeKind::VarDecl,
        _ => RSNodeKind::Unexposed,
    };

    info!("node_get_kind: {:?}", kind);
    kind
}

#[no_mangle]
pub unsafe extern "C" fn node_get_spelling_name(
    node: *mut RSNode,
) -> *const ::std::os::raw::c_char {
    if let Some(i) = (*node).node.get_ident() {
        CString::new(i.to_string()).unwrap().into_raw()
    } else if let Node::Type(Type::Path(path)) = &(*node).node {
        let s = path.into_token_stream().to_string().replace(" ", "");
        CString::new(s).unwrap().into_raw()
    } else if let Node::Path(path) = &(*node).node {
        let s = path.into_token_stream().to_string().replace(" ", "");
        CString::new(s).unwrap().into_raw()
    } else {
        ptr::null()
    }
}

#[no_mangle]
pub unsafe extern "C" fn node_get_spelling_range(node: *mut RSNode) -> RSRange {
    if let Some(i) = (*node).node.get_ident() {
        return i.span().into();
    } else {
        RSRange::invalid()
    }
}

#[no_mangle]
pub unsafe extern "C" fn node_get_extent(node: *mut RSNode) -> RSRange {
    if let Some(span) = (*node).node.get_span() {
        span.into()
    } else {
        RSRange::invalid()
    }
}

#[no_mangle]
pub unsafe extern "C" fn node_get_hash(node: *mut RSNode) -> u64 {
    let mut hasher = DefaultHasher::new();
    (*node).node.hash(&mut hasher);
    hasher.finish()
}

#[no_mangle]
pub unsafe extern "C" fn node_is_equal(one: *mut RSNode, other: *mut RSNode) -> bool {
    (*one).node == (*other).node
}

#[no_mangle]
pub unsafe extern "C" fn node_clone(node: *mut RSNode) -> *mut RSNode {
    Box::into_raw(Box::new(RSNode {
        node: (*node).node.clone(),
        crate_: (*node).crate_,
    }))
}

#[no_mangle]
pub unsafe extern "C" fn destroy_string(str: *mut ::std::os::raw::c_char) {
    if !str.is_null() {
        CString::from_raw(str);
    }
}

#[no_mangle]
pub unsafe extern "C" fn crate_get_diagnostics(crate_: *mut RSCrate) -> *mut RSDiagnosticIterator {
    ptr::null_mut()
}

#[no_mangle]
pub unsafe extern "C" fn destroy_diagnostic_iterator(iterator: *mut RSDiagnosticIterator) {}

#[no_mangle]
pub unsafe extern "C" fn diagnostics_next(
    iterator: *mut RSDiagnosticIterator,
) -> *mut RSDiagnostic {
    ptr::null_mut()
}

#[no_mangle]
pub unsafe extern "C" fn destroy_diagnostic(diagnostic: *mut RSDiagnostic) {}

#[no_mangle]
pub unsafe extern "C" fn diagnostic_get_level(diagnostic: *mut RSDiagnostic) -> RSDiagnosticLevel {
    RSDiagnosticLevel::Info
}

#[no_mangle]
pub unsafe extern "C" fn diagnostic_get_message(
    diagnostic: *mut RSDiagnostic,
) -> *const ::std::os::raw::c_char {
    ptr::null()
}

#[no_mangle]
pub unsafe extern "C" fn diagnostic_get_primary_range(diagnostic: *mut RSDiagnostic) -> RSRange {
    RSRange::invalid()
}

macro_rules! visit {
    ($parent:expr, $node:expr, $cb:expr, $data:expr) => {
        {
            let n = RSNode {
                crate_: (*$parent).crate_,
                node: $node,
            };

            let res = $cb(&n, $parent, $data);
            match res {
                RSVisitResult::Recurse => {
                    visit_children(&n, Some($cb), $data);
                }
                RSVisitResult::Continue => {},
                RSVisitResult::Break => {
                    error!("RSVisitResult::Break");
                    return;
                }
            };
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn visit_children(
    node: *const RSNode,
    callback: CallbackFn,
    data: *mut ::std::os::raw::c_void,
) {
    let callback = if let Some(cb) = callback {
        cb
    } else {
        return;
    };

    match &(*node).node {
        Node::Item(item) => match item {
            Item::Impl(i) => {
                visit!(node, Node::Type(*i.self_ty.clone()), callback, data);
                for item in &i.items {
                    visit!(node, Node::ImplItem(item.clone()), callback, data);
                }
            }
            Item::Struct(s) => for field in &s.fields {
                visit!(node, Node::Field(field.clone()), callback, data);
            },
            Item::Enum(e) => for variant in &e.variants {
                visit!(node, Node::Variant(variant.clone()), callback, data);
            },
            Item::Fn(f) => {
                if let ReturnType::Type(_, t) = &f.decl.output {
                    visit!(node, Node::Type(*t.clone()), callback, data);
                }
                for input in &f.decl.inputs {
                    info!("Function input: {:?}", input);
                }
                for statement in &f.block.stmts {
                    match statement {
                        Stmt::Local(l) => {
                            visit!(node, Node::Local(l.clone()), callback, data);
                        }
                        Stmt::Item(i) => {
                            visit!(node, Node::Item(i.clone()), callback, data);
                        }
                        Stmt::Expr(e) | Stmt::Semi(e, _) => {
                            visit!(node, Node::Expr(e.clone()), callback, data);
                        }
                    }
                }
            }
            _ => {}
        },
        Node::ImplItem(i) => match &i {
            ImplItem::Method(m) => {
                if let ReturnType::Type(_, t) = &m.sig.decl.output {
                    visit!(node, Node::Type(*t.clone()), callback, data);
                }
                for input in &m.sig.decl.inputs {
                    info!("Function input: {:?}", input);
                }
                for statement in &m.block.stmts {
                    match statement {
                        Stmt::Local(l) => {
                            visit!(node, Node::Local(l.clone()), callback, data);
                        }
                        Stmt::Item(i) => {
                            visit!(node, Node::Item(i.clone()), callback, data);
                        }
                        Stmt::Expr(e) | Stmt::Semi(e, _) => {
                            visit!(node, Node::Expr(e.clone()), callback, data);
                        }
                    }
                }
            }
            _ => {}
        },
        Node::File(file) => for item in &file.items {
            visit!(node, Node::Item(item.clone()), callback, data);
        },
        Node::Variant(variant) => for field in &variant.fields {
            visit!(node, Node::Field(field.clone()), callback, data);
        },
        Node::Field(field) => {
            visit!(node, Node::Type(field.ty.clone()), callback, data);
        }
        Node::Type(t) => match &t {
            Type::Path(p) => for segment in &p.path.segments {
                visit!(node, Node::PathSegment(segment.clone()), callback, data);
            },
            Type::Slice(s) => visit!(node, Node::Type(*(s.elem).clone()), callback, data),
            Type::Array(a) => visit!(node, Node::Type(*(a.elem).clone()), callback, data),
            Type::Ptr(p) => visit!(node, Node::Type(*(p.elem).clone()), callback, data),
            Type::Reference(r) => visit!(node, Node::Type(*(r.elem).clone()), callback, data),
            Type::Tuple(t) => for elem in &t.elems {
                visit!(node, Node::Type(elem.clone()), callback, data);
            },

            _ => {}
        },
        Node::Path(p) => for segment in &p.segments {
            visit!(node, Node::PathSegment(segment.clone()), callback, data);
        },
        Node::Expr(e) => match e {
            Expr::Struct(s) => {
                visit!(node, Node::Path(s.path.clone()), callback, data);
                for f in &s.fields {
                    visit!(node, Node::Expr(f.expr.clone()), callback, data);
                }
            }
            Expr::Return(r) => {
                if let Some(e) = &r.expr {
                    visit!(node, Node::Expr(*e.clone()), callback, data);
                }
            }
            _ => {
                info!("Unhandled expression {:?}", e);
            }
        },
        Node::Local(l) => {
            if let Some((_, t)) = &l.ty {
                visit!(node, Node::Type(*t.clone()), callback, data);
            }
            if let Some((_, e)) = &l.init {
                visit!(node, Node::Expr(*e.clone()), callback, data);
            }
        }
        _ => {}
    }
}
